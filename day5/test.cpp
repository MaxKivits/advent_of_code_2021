
#include <bits/stdc++.h>

struct Point
{
    int x;
    int y;
    Point(int &x_, int &y_) : x(x_), y(y_){};
    Point()
    {
        x = -1;
        y = -1;
    }
};

struct Line
{
    Point start;
    Point end;
    Line(std::string &in)
    {
        std::stringstream ss(in);
        char ign;
        ss >> start.x >> ign >> start.y >> ign >> ign >> end.x >> ign >> end.y;
    }
    bool x_line()
    {
        return (start.x - end.x);
    }
    void to_string()
    {
        std::cout << "start - " << start.x << "," << start.y << " end - " << end.x << "," << end.y << std::endl;
    }
};

int main()
{

    std::vector<Line> lines;
    std::vector<std::string> in{"0,9 -> 5,9", "8,0 -> 0,8", "9,4 -> 3,4", "2,2 -> 2,1", "7,0 -> 7,4", "6,4 -> 2,0", "0,9 -> 2,9", "3,4 -> 1,4", "0,0 -> 8,8", "5,5 -> 8,2"};
    for (auto &el : in)
    {
        lines.push_back(Line(el));
    }
    for (auto &line : lines)

    {
        line.to_string();
        std::cout << line.x_line() << std::endl;
    }

    return 1;
}
