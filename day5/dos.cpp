#include <bits/stdc++.h>

// constexpr size_t diagram_length = 10;
size_t diagram_length = 1000;

template <typename T>
int sgn(T val)
{
    return (T(0) < val) - (val < T(0));
}

struct Point
{
    int x;
    int y;
    // Point(int &x_, int &y_) : x(x_), y(y_){};
    Point(const int &x_, const int &y_)
    {
        x = x_;
        y = y_;
    }
    Point()
    {
        x = -1;
        y = -1;
    }
};
// calc distance between 2 points
int distance(const Point &p1, const Point &p2)
{
    return std::abs(p1.x - p2.x + p1.y - p2.y);
}
struct Line
{
    Point start;
    Point end;
    Line(std::string &in)
    {
        std::stringstream ss(in);
        char ign;
        ss >> start.x >> ign >> start.y >> ign >> ign >> end.x >> ign >> end.y;
    }
    bool is_straight() const
    {
        return (start.x == end.x || start.y == end.y);
    }
    // check if line is along x axis
    bool x_line() const
    {
        return (start.x - end.x);
    }
    // Return vector of all points along line
    std::vector<Point> all_points() const
    {
        std::vector<Point> out;
        if (this->is_straight())
        {
            for (int i = 0; i <= distance(this->start, this->end); i++)
            {
                if (this->x_line())
                {
                    if (start.x < end.x)
                    {
                        out.push_back(Point(start.x + i, start.y));
                    }
                    else
                    {
                        out.push_back(Point(end.x + i, start.y));
                    }
                }
                else
                {
                    if (start.y < end.y)
                    {
                        out.push_back(Point(start.x, start.y + i));
                    }
                    else
                    {
                        out.push_back(Point(start.x, end.y + i));
                    }
                }
            }
            return out;
        }
        else
        {
            // line is diagonal
            int x_diff = end.x - start.x;
            int y_diff = end.y - start.y;

            for (size_t i = 0; i <= sgn(x_diff) * x_diff; i++)
            {
                out.push_back(Point(start.x + sgn(x_diff) * i, start.y + sgn(y_diff) * i));
            }
            return out;
        }
    }
    void to_string() const
    {
        std::cout << "start - " << start.x << "," << start.y << " end - " << end.x << "," << end.y << std::endl;
    }
};

void diagram_draw_line(std::vector<int> &diagram, const Line &line)
{
    auto points = line.all_points();
    for (const auto &point : points)
    {
        diagram[point.x + point.y * diagram_length] += 1;
    }
}

void diagram_print(std::vector<int> &diagram)
{
    for (size_t y = 0; y < diagram_length; y++)
    {
        for (size_t x = 0; x < diagram_length; x++)
        {
            std::cout << diagram[x + y * diagram_length];
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

int main()
{
    // Speed up IO
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);

    // point cin to input.txt
    // std::ifstream input("test.txt");
    std::ifstream input("input.txt");
    std::cin.rdbuf(input.rdbuf());

    // program states
    std::vector<int>
        diagram(diagram_length * diagram_length, 0);
    std::vector<Line> lines;

    // IO
    std::string input_str;
    while (std::getline(std::cin, input_str))
    {
        lines.push_back(Line(input_str));
    }

    // only consider straight lines
    // for (size_t i = 0; i < lines.size(); i++)
    // {
    //     if (!lines[i].is_straight())
    //     {
    //         lines.erase(lines.begin() + i);
    //         i--;
    //     }
    // }
    for (const auto &line : lines)
    {
        // line.to_string();
        diagram_draw_line(diagram, line);
        // std::cout << "diagram sum: " << std::accumulate(diagram.begin(), diagram.end(), 0) << std::endl;
        // diagram_print(diagram);
    }

    diagram_print(diagram);
    std::cout << "answer part 1 - number of gridpoints >= 2: " << std::count_if(diagram.begin(), diagram.end(), [](int el) -> bool
                                                                                { return el > 1; })
              << std::endl;

    return 1;
}