

#include<bits/stdc++.h>


int main() {
    
    std::ifstream cin("input.txt");

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);

    std::cin.rdbuf(cin.rdbuf());

    int prev(60000);
    int counter(0);
    
    std::string line;
    while(getline(std::cin,line))
    {
        std::istringstream is(line);
        int el;
        while( is >> el) {
            if(el > prev)
            {
                counter++;
            }
        }
        prev = el;
    }
    std::cout << counter << std::endl;
    return 0;    
}
