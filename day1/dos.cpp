

#include<bits/stdc++.h>

int summit (std::deque<int> window)
{
    int sum =0;
    for(const auto & el:window)
    {
        sum +=el;
    }
    return sum;
}

int main() {
    
    std::ifstream cin("input.txt");

    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);

    std::cin.rdbuf(cin.rdbuf());

    int counter(0);

    std::deque<int> window;
    for(int i = 0;i<3;i++)
    {
        std::string line;
        std::getline(std::cin,line);
        std::istringstream is(line);
        int el;
        while(is>>el) {
            window.push_back(el);
        }
    }
    int prevsum = summit(window);
    int sum;

    
    std::string line;
    while(getline(std::cin,line))
    {
        std::istringstream is(line);
        int el;
        while( is >> el) {
            window.push_back(el);
            window.pop_front();
            sum = summit(window);
            if(sum > prevsum)
            {
                counter++;
            }
        }
        prevsum = sum;
    }
    std::cout << counter << std::endl;
    return 0;    
}
