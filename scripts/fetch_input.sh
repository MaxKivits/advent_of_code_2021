#! /bin/bash

# Downloads today's Advent of Code input file or a previous day

DAY=$1      # Optional day arg

# take today if no date given
if [ -z "$DAY" ]; then
    DAY=$(date +%-d)
fi

YEAR="2021"

FILE_URL="https://adventofcode.com/$YEAR/day/$DAY/input"
echo "Target file is at: ${FILE_URL}..."

# Find this session cookie var: log into adventofcode.com, inspect -> applications -> session
SESSION="53616c7465645f5f6794ca4cebcedc8de15e77add69562280be19c0bbdafeb99e12d9a42b569d8d5c178cbaf2e0351d6"
# Https request the file
curl https://adventofcode.com/$YEAR/day/$DAY/input --cookie "session=$SESSION" > input.txt
