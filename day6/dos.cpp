#include <bits/stdc++.h>
#include "infint/InfInt.h"

// all mutations that happen in a single day
std::array<InfInt, 9> a_fishy_day_goes_by(std::array<InfInt, 9> &states)
{
    std::array<InfInt, 9> new_states{0};
    for (size_t i = 8; i >= 1; i--)
    {
        new_states[i - 1] = states[i];
    }
    // loop zero states, new babies are born
    new_states[6] += states[0];
    new_states[8] += states[0];
    return new_states;
}

int main()
{
    // Speed up IO
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);

    // point cin to input.txt
    // std::ifstream input("test.txt");
    std::ifstream input("input.txt");
    std::cin.rdbuf(input.rdbuf());

    // States
    std::array<InfInt, 9> states;
    states.fill(0);

    // IO
    std::string line;
    while (std::getline(std::cin, line, ','))
    {
        states[std::stoi(line)]++;
    }

    // main loop
    std::vector<InfInt> fishcounter;
    std::vector<uint64_t> dummycounter;
    size_t max_days = 256;
    for (size_t days = 0; days < max_days; days++)
    {
        states = a_fishy_day_goes_by(states);
        InfInt fish_total = 0;
        for (size_t i = 0; i < 9; i++)
        {
            fish_total += states[i];
        }
        fishcounter.push_back(fish_total);
        dummycounter.push_back(fish_total.toLongLong());
    }
    // sum

    std::cout << "answer part 2 - lanternfish after " << max_days << " days:\n"
              << fishcounter[max_days - 1].toString() << std::endl;

    return 0;
}