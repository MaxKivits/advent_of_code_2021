#include <bits/stdc++.h>

// constexpr uint8_t offset = 2;

// all mutations that happen in a single day
std::array<uint64_t, 9> a_fishy_day_goes_by(std::array<uint64_t, 9> &states)
{
    std::array<uint64_t, 9> new_states{0};
    for (size_t i = 8; i >= 1; i--)
    {
        new_states[i - 1] = states[i];
    }
    // loop zero states, new babies are born
    new_states[6] += states[0];
    new_states[8] += states[0];
    return new_states;
}

int main()
{
    // Speed up IO
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);

    // point cin to input.txt
    std::ifstream input("test.txt");
    // std::ifstream input("input.txt");
    std::cin.rdbuf(input.rdbuf());

    // States
    std::array<uint64_t, 9> states{0};

    // IO
    std::string line;
    while (std::getline(std::cin, line, ','))
    {
        states[std::stoi(line)]++;
    }

    // main loop
    std::vector<uint64_t> fishcounter;
    size_t max_days = 80;
    for (size_t days = 0; days < max_days; days++)
    {
        states = a_fishy_day_goes_by(states);
        fishcounter.push_back(std::accumulate(states.begin(), states.end(), 0));
    }
    std::cout << "fish left after 18 days: " << fishcounter[17] << std::endl;
    std::cout << "answer part 1 - lanternfish after " << max_days << " days:\n"
              << std::accumulate(states.begin(), states.end(), 0) << std::endl;

    return 0;
}