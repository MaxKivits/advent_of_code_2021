#include<bits/stdc++.h>

int main() {

    // faster IO
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);

    // point cin to input.txt
    std::ifstream input("input.txt");
    std::cin.rdbuf(input.rdbuf());
    
    // 12 ints, 1 for each row in input
    std::vector<int> list(12,0);
    
    // track number of lines in input file
    int lines(0);

    // IO
    std::string line;
    while(std::getline(std::cin,line)) {
        // logic: count 1s in each column
        for(int i =0;i<12;i++) {
            // char meme
            list[i]+=line[i]-'0';
        }
        lines++;
    }
    
    // calc if average bit is 1 or 0 for every row
    std::transform(list.begin(),list.end(),list.begin(),[&lines](auto &el)->int{return ((el>lines/2)?1:0);});
    
    // fold list back to epsilon rate
    uint16_t epsilon_rate = std::accumulate(list.begin(),list.end(),0,[](int &el, int out)->int{return (el<<1)+out;});
    
    // gamma rate
    uint16_t gamma_rate = (~epsilon_rate)&0b0000111111111111;

    // output
    std::cout << "Answer Part 1:\n" << epsilon_rate*gamma_rate <<std::endl;    
    return 0;
}