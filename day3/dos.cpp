#include <bits/stdc++.h>
int avg_bit_at_index(std::vector<std::string> &input_list, int &index)
{
    double avg(0);
    for (int i(0); i < input_list.size(); i++)
    {
        avg += input_list[i][index]-'0';
    }
    return(avg >= (input_list.size() / 2.0) ? 1 : 0);
}

// void search_list(std::vector<std::string> &inputs, std::vector<int> &avg_bit_list)
// {
//     int i(0);
//     while (inputs.size() > 1)
//     {
//         for (int j(0); j < inputs.size(); j++)
//         {
//             if ((inputs[j][i] - '0' != avg_bit_list[i]))
//             {
//                 inputs.erase(inputs.begin() + j);
//                 j--;
//             }
//         }
//         i++;
//     }
// }

void search_oxy(std::vector<std::string> &inputs)
{
    int i(0);
    while (inputs.size() > 1)
    {
        int avg_bit = avg_bit_at_index(inputs,i);
        for (int j(0); j < inputs.size(); j++)
        {
            if ((inputs[j][i] - '0' != avg_bit))
            {
                inputs.erase(inputs.begin() + j);
                j--;
            }
        }
        i++;
    }
}
void search_co2(std::vector<std::string> &inputs)
{
    int i(0);
    while (inputs.size() > 1)
    {
        int avg_bit = avg_bit_at_index(inputs,i);
        for (int j(0); j < inputs.size(); j++)
        {
            if ((inputs[j][i] - '0' == avg_bit))
            {
                inputs.erase(inputs.begin() + j);
                j--;
            }
        }
        i++;
    }
}


int main()
{
    // faster IO
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);

    // point cin to input.txt
    std::ifstream input("input.txt");
    const int input_size = 12;
    // std::ifstream input("test.txt");
    // const int input_size = 5;
    std::cin.rdbuf(input.rdbuf());

    // list of inputs, to be processed so only oxy# remains
    std::vector<std::string> oxy_list;

    // track number of lines in input file
    int lines(0);

    // IO
    std::string line;
    while (std::getline(std::cin, line))
    {
        oxy_list.push_back(line);
        lines++;
    }
    // list of inputs, to be processed so only co2# remains
    std::vector<std::string> co2_list(oxy_list);

    // 12 ints, 1 for each row in input
    std::vector<int> list(input_size, 0);

    for (int i(0); i < oxy_list.size(); i++)
    {
        for (int j(0); j < input_size; j++)
        {
            list[j] += oxy_list[i][j] - '0';
        }
    }

    std::cout << "hold" << std::endl;

    // calc if average bit is 1 or 0 for every row
    std::transform(list.begin(), list.end(), list.begin(), [&lines](auto &el) -> int
                   { return ((el >= lines / 2) ? 1 : 0); });

    // the meat
    search_oxy(oxy_list);
    // std::vector<int> list_inverse(list);
    // std::transform(list.begin(), list.end(), list_inverse.begin(), [](auto &el) -> int
                //    { return !el; });
    search_co2(co2_list);

    std::cout << "Answer Part 2 should now be found :)\n"
              << "avg bits: ";
    std::copy(list.begin(), list.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << "\nOxygen rating: " << (*oxy_list.begin()) << " = " << std::stoi((*oxy_list.begin()), nullptr, 2) << std::endl
              << "co2gen rating: " << (*co2_list.begin()) << " = " << std::stoi((*co2_list.begin()), nullptr, 2) << std::endl
              << std::stoi((*oxy_list.begin()), nullptr, 2) * std::stoi((*co2_list.begin()), nullptr, 2) << std::endl;

    return 0;
}