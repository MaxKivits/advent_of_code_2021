#include <bits/stdc++.h>

int main()
{
    // Speed up IO
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);

    // point cin to input.txt
    // std::ifstream input("test.txt");
    std::ifstream input("input.txt");
    std::cin.rdbuf(input.rdbuf());

    // state
    std::vector<int> positions;

    // IO
    std::string line;
    std::getline(std::cin,line);
    std::stringstream ss(line);
    int token;
    char delim;
    while (ss >> token)
    {
        positions.push_back(token);
        ss >> delim;
    }
    
    // find median
    std::nth_element(positions.begin(),positions.begin()+positions.size()/2,positions.end());
    std::cout << "median: " << positions[positions.size()/2] << std::endl;

    // calc distance of elements to median
    int fuel_cost(0);
    for (size_t i = 0; i < positions.size(); i++)
    {
        fuel_cost += std::abs(positions[i]-positions[positions.size()/2]);
    }
    
    std::cout << "answer part 1: " << fuel_cost << std::endl;

    return 0;
}