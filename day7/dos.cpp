#include <bits/stdc++.h>

int main()
{
    // Speed up IO
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);

    // point cin to input.txt
    // std::ifstream input("test.txt");
    std::ifstream input("input.txt");
    std::cin.rdbuf(input.rdbuf());

    // state
    std::vector<int> positions;

    // IO
    std::string line;
    std::getline(std::cin,line);
    std::stringstream ss(line);
    int token;
    char delim;
    while (ss >> token)
    {
        positions.push_back(token);
        ss >> delim;
    }
    std::cout << "size of input: " << positions.size() << std::endl;

    
    // find median
    std::nth_element(positions.begin(),positions.begin()+positions.size()/2,positions.end());
    std::cout << "median: " << positions[positions.size()/2] << std::endl;

    // find average
    double avg = std::reduce(positions.begin(),positions.end());
    std::cout << "average: " << avg/positions.size() << std::endl;
    int avg_int_low = static_cast<int>((avg/positions.size())-0.5);
    int avg_int_max = static_cast<int>((avg/positions.size())+0.5);
    std::cout << "average as int i low: " << avg_int_low << " max: " << avg_int_max << std::endl;

    // calc distance of elements to average
    uint64_t fuel_cost_max = 0;
    uint64_t fuel_cost_low = 0;
    for (size_t i = 0; i < positions.size(); i++)
    {
        int distance_low = std::abs(positions[i]-avg_int_low);
        // triangular number sum 
        fuel_cost_low += (distance_low+std::pow(distance_low,2))/2;
        int distance_max = std::abs(positions[i]-avg_int_max);
        // triangular number sum 
        fuel_cost_max += (distance_max+std::pow(distance_max,2))/2;
    }
    
    std::cout << "answer part 2 - low: " << fuel_cost_low << " max: " << fuel_cost_max  << std::endl;

    return 0;
}