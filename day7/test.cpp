

#include <bits/stdc++.h>

template <typename T>
void printVector(std::vector<T> in)
{
    for (const auto &el : in)
    {
        std::cout << el << " ";
    }
    std::cout << std::endl;
}

int main()
{
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);

    std::vector<int> cas(10, 1);
    int max_num = 98;
    cas.push_back(max_num);

    // median
    std::cout << "median:" << std::endl;
    std::nth_element(cas.begin(), cas.begin() + (cas.size() / 2), cas.end());
    std::cout << cas[cas.size() / 2] << std::endl;

    // find average
    double avg = std::reduce(cas.begin(), cas.end());
    std::cout << "average: " << avg / cas.size() << std::endl;
    int avg_int = static_cast<int>((avg / cas.size()) + 0.5);
    std::cout << "average as int: " << avg_int << std::endl;

    int avg_int_low = static_cast<int>((avg/cas.size())-0.5);
    int avg_int_max = static_cast<int>((avg/cas.size())+0.5);
    std::cout << "low: " <<  avg_int_low << " max: " << avg_int_max  << std::endl;

    // calc distance of elements to average
    std::vector<int> fuel_cost_list;
    for (int j = 0; j < max_num; j++)
    {
        uint64_t fuel_cost = 0;
        for (size_t i = 0; i < cas.size(); i++)
        {
            int distance = std::abs(cas[i] - j);
            // triangular number sum
            fuel_cost += (distance + std::pow(distance, 2)) / 2;
        }

        std::cout << "i = " << j << " cost: " << fuel_cost << std::endl;
        fuel_cost_list.push_back(fuel_cost);
    }

    std::vector<int> fuel_cost_list_sort(fuel_cost_list);
    std::sort(fuel_cost_list_sort.begin(), fuel_cost_list_sort.end());
    std::cout << "min cost of all iterations: " << fuel_cost_list_sort[0] << " at iteration: " << std::endl;

    return 0;
}
