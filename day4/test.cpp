#include <bits/stdc++.h>

void fish(std::vector<int> in)
{
    in.push_back(6);
}

template <typename T>
void printVector(std::vector<T> in)
{
    for (const auto &el : in)
    {
        std::cout << el << " ";
    }
    std::cout << std::endl;
}

// bool check_row_win(std::vector::iterator)
template <class T>
void transpose_inplace (std::array<T,9> &in,int width,int length)
{
    std::array<T,9> tmp(in);
    for (size_t i = 0; i < width; i++)
    {
        for (size_t j = 0; j < length; j++)
        {
            tmp[i+j*3] = in[j+i*3];
        }
    }
    in = tmp;
}
template <class T>
void transpose_inplace (std::array<T,25> &in,int width,int length)
{
    std::array<T,25> tmp(in);
    for (size_t i = 0; i < width; i++)
    {
        for (size_t j = 0; j < length; j++)
        {
            tmp[i+j*5] = in[j+i*5];
        }
    }
    in = tmp;
}

bool check_win_state(std::array<bool, 25> current_state)
{
    bool win = false;
    // check rows
    for (size_t i = 0; i < 5; i++)
    {
        if (std::accumulate(current_state.begin() + (i * 5), current_state.begin() + 5 + (i * 5), 1, [](bool left, bool right) -> bool
                            { return left & right; }))
        {
            win = true;
            return win;
        }
    }
    // check colums
    transpose_inplace(current_state,5,5);
    for (size_t i = 0; i < 5; i++)
    {
        if (std::accumulate(current_state.begin() + (i * 5), current_state.begin() + 5 + (i * 5), 1, [](bool left, bool right) -> bool
                            { return left & right; }))
        {
            win = true;
            return win;
        }
    }


    return win;
}


int main()
{
    std::array<int,9> meme{0,1,2,3,4,5,6,7,8};
    std::vector<bool> test1{1, 1, 1, 0, 1};
    std::array<bool,25> current_state{1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0};
    // std::cout << current_state.size() << std::endl;
    // printVector(current_state);
    // for (size_t i = 0; i < 5; i++)
    // {
    //     if (std::accumulate(current_state.begin() + (i * 5), current_state.begin() + 5 + (i * 5), 1, [](bool left, bool right) -> bool
    //                         { return left & right; }))
    //     {
    //         std::cout << "WON ROW #: " << i << std::endl;
    //     }
    // }

    transpose_inplace(meme,3,3);
    for(const auto &el:meme)
    {
        std::cout << el << " ";
    }


    std::cout << "\n~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;


    auto win = check_win_state(current_state);
    std::cout << "WON??? - " << win << std::endl;

    return 1;
}