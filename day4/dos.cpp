#include <bits/stdc++.h>

// heel goede transpose functie
template <class T>
void transpose_inplace(std::array<T, 25> &in, int width, int length)
{
    std::array<T, 25> tmp(in);
    for (size_t i = 0; i < width; i++)
    {
        for (size_t j = 0; j < length; j++)
        {
            tmp[i + j * 5] = in[j + i * 5];
        }
    }
    in = tmp;
}

// Reads new board from input
int read_board(std::array<int, 25> &current_board, std::array<bool, 25> &current_state)
{
    // reset board state
    current_state.fill(false);
    // IO
    std::string line;
    std::getline(std::cin, line);
    if (!std::cin.eof())
    {
        for (size_t i = 0; i < 5; i++)
        {
            std::getline(std::cin, line);
            std::stringstream ss(line);
            int number(0), j(0);
            while (ss >> number)
            {
                current_board[(i * 5) + j] = number;
                j++;
            }
        }
        return 1;
    }
    else
    {
        std::cout << "EOF reached" << std::endl;
        return 0;
    }
}

// check if a win is currently present
bool check_win_state(std::array<bool, 25> current_state)
{
    // check rows
    for (size_t i = 0; i < 5; i++)
    {
        if (std::accumulate(current_state.begin() + (i * 5), current_state.begin() + 5 + (i * 5), 1, [](bool left, bool right) -> bool
                            { return left & right; }))
        {
            return true;
        }
    }
    // check colums
    transpose_inplace(current_state, 5, 5);
    for (size_t i = 0; i < 5; i++)
    {
        if (std::accumulate(current_state.begin() + (i * 5), current_state.begin() + 5 + (i * 5), 1, [](bool left, bool right) -> bool
                            { return left & right; }))
        {
            return true;
        }
    }
    return false;
}
// play a round of bingo :)
std::tuple<int, int> play_bingo(std::array<int, 25> &current_board, std::array<bool, 25> &current_state, std::vector<int> &bingo_input, int &max_steps_to_win)
{
    // steps to win current board
    int steps(0);
    int final_number(0);

    while (steps < bingo_input.size())
    {
        // see if drawn number is on board
        auto itr = std::find(current_board.begin(), current_board.end(), bingo_input[steps]);
        if (itr != current_board.end())
        {
            // if it is -> update board state to reflect position
            current_state[itr - current_board.begin()] = true;
            // check for win
            if (check_win_state(current_state))
            {
                final_number = bingo_input[steps];
                return std::make_tuple(steps, final_number);
            }
        }
        // if (steps == 99)
        // {
        //     // end of input reached -> no win on this board
        //     return std::make_tuple(9999999, final_number);
        // }
        // else
        // {
        steps++;
        // }
    }
    // took more than max steps -> move on
    return std::make_tuple(-1, -1);
}

int main()
{
    // Speed up IO
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);

    // point cin to input.txt
    // std::ifstream input("test.txt");
    std::ifstream input("input.txt");
    std::cin.rdbuf(input.rdbuf());

    // Bingo numbers list
    std::vector<int> bingo_input;

    //  IO bingo numbers
    std::string first_line;
    std::getline(std::cin, first_line);

    std::stringstream ss(first_line);
    std::string input_number;
    while (std::getline(ss, input_number, ','))
    {
        bingo_input.push_back(std::stoi(input_number));
    }

    // Bingo board states
    std::array<int, 25> current_board;
    std::array<bool, 25> current_state;

    std::array<int, 25> worst_board;
    std::array<bool, 25> worst_state;

    // minimum bingo numbers drawn to win, tracked over all boards
    int max_steps_to_win = 0;
    int current_steps_to_win = 0;
    int current_final_number(0);
    int worst_final_number(0);

    // count boards
    int i = 0;
    // main loop
    while (read_board(current_board, current_state))
    {
        // play bingo on the current board, find steps to win
        std::tie(current_steps_to_win, current_final_number) = play_bingo(current_board, current_state, bingo_input, max_steps_to_win);

        // assuming only 1 board has minimum steps
        if (current_steps_to_win > max_steps_to_win)
        {
            // save best board
            max_steps_to_win = current_steps_to_win;
            worst_board = current_board;
            worst_state = current_state;
            worst_final_number = current_final_number;
        }
        i++;
    }
    // find score of unmarked numbers
    std::cout << "done with main loop, boards processed: " << i << std::endl;
    std::cout << "max steps to win: " << max_steps_to_win << std::endl;
    std::cout << "Final number: " << worst_final_number << std::endl;
    std::cout << "winning board: " << std::endl;
    for (size_t i = 0; i < 5; i++)
    {
        for (size_t j = 0; j < 5; j++)
        {
            std::cout << worst_board[i * 5 + j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "winning board state: " << std::endl;
    for (size_t i = 0; i < 5; i++)
    {
        for (size_t j = 0; j < 5; j++)
        {
            std::cout << worst_state[i * 5 + j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "sum of UNmarked numbers on winning board:" << std::endl;
    auto worst_board_copy(worst_board);
    for (size_t i = 0; i < 25; i++)
    {
        worst_board_copy[i] *= !worst_state[i];
    }
    auto sum = std::accumulate(worst_board_copy.begin(), worst_board_copy.end(), 0);
    std::cout << sum << std::endl;
    
    // print out answer
    std::cout << "Answer part 2: " << std::endl;
    std::cout << sum*worst_final_number << std::endl;

    return 0;
}
