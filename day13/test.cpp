#include <bits/stdc++.h>

struct Point
{
    int x;
    int y;
    Point(int &_x, int &_y)
    {
        x = _x;
        y = _y;
    }
    Point(std::string in)
    {
        std::stringstream ss(in);
        ss >> x >> y;
    }
    friend std::ostream& operator<<(std::ostream &out, const Point& p)
    {
        out << "x: " << p.x << " y: " << p.y << std::endl;
        return out;
    }
};

void print_point_set(std::unordered_set<std::string> &point_set)
{
    for (std::unordered_set<std::string>::iterator it = point_set.begin(); it != point_set.end(); it++)
    {
        std::cout << *it << std::endl;
    }
}

int main()
{
    // std::vector<char> test(10,'.');
    // std::vector<std::vector<char>> test2d (10,std::vector<char>(10,'.'));
    
    std::unordered_set<std::string> point_set{"123213","12f8ys","2asidufas"};
    for (std::unordered_set<std::string>::iterator it = point_set.begin(); it != point_set.end(); it++)
    {
        std::cout << *it << std::endl;
    }

    print_point_set(point_set);

    return 1;
}
