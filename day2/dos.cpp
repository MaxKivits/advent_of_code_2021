#include<bits/stdc++.h>

int main() {
    // faster IO
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);

    // point cin to input.txt
    std::ifstream input("input.txt");
    std::cin.rdbuf(input.rdbuf());

    // start solution
    int pos(0),depth(0),aim(0);
    
    // map directions
    std::map<std::string,int> map {
        {"down",1},
        {"up",-1}
    };

    // IO 
    std::string line;
    while(std::getline(std::cin,line)) {
        std::stringstream ss(line);
        std::string dir;
        int value;
        ss >> dir >> value;

        // logic
        if(!dir.compare("forward")) {
            pos+=value;
            depth+=aim*value;
        }
        else {
            aim += map[dir] * value;
        }
        
    }

    // print
    std::cout << "p1: " << pos*depth << "\n";

    return 0;
}